<?php

namespace App\Http\Controllers;
use SoapClient;
use File;
use Illuminate\Http\Request;

class TestWsdlController extends Controller
{
  public function index(){
      try {
    //The FULL path to the location of your WSDL file.
    $fullPathToWsdl = public_path('/sample_xml.xml');
    // dd($fullPathToWsdl);
    // 'http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL';

    //The URL of the web service.
    $url = 'http://wsf.cdyne.com/';

    //Instantiate the SoapClient object.
    // $client = new SoapClient(
    //     $fullPathToWsdl,
    //     array(
    //         'location' => $url,
    //         'trace' => 1
    //     )
    // );
    // $wsdlUrl = self::getWsdl();
    // $soapClientOptions = [
    //     'stream_context' => self::generateContext(),
    //     'cache_wsdl'     => WSDL_CACHE_NONE
    // ];
    $client = new SoapClient('http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL', array('cache_wsdl' => WSDL_CACHE_NONE,'trace' => 1) );

    dd($client);
  }catch(\Exception $e) {
      return $e->getMessage();
  }
}
}
