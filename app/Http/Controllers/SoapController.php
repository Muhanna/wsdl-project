<?php
namespace App\Http\Controllers;

class SoapController extends BaseSoapController
{
    private $service;

    public function index(){
        return view ('services');
    }

    public function services(){
        try {
            self::setWsdl('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl');
            $this->service = InstanceSoapClient::init();

            $countryCode = 'DK';
            $vatNumber = '47458714';

            $params = [
                'countryCode' => request()->input('countryCode') ? request()->input('countryCode') : $countryCode,
                'vatNumber'   => request()->input('vatNumber') ? request()->input('vatNumber') : $vatNumber
            ];
            $response = $this->service->checkVat($params);
            return view ('wsdl-services', compact('response'));
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }


}
